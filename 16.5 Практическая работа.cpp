﻿#include <iostream>
#include <time.h>


int main()
{

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	


	int day = buf.tm_mday;
	const int Size = 5;
	int RowIndex = day % Size;
	int array[Size][Size];
	int SumRow = 0;

	for (int i = 0; i < Size; i++)
	{
		for (int j = 0; j < Size; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}

	for (int Q = 0; Q < Size; Q++)
	{
		SumRow += array[RowIndex][Q];
	}
	std::cout << "RowIndex = " << RowIndex << "\n";
	std::cout << "SumRow = "<< SumRow;
}
